package br.edu.unisep.store.domain.validator;

import br.edu.unisep.store.domain.dto.RegisterCustomerDto;
import org.apache.commons.lang3.Validate;


public class CustomerValidator {

    public void validate(RegisterCustomerDto registerCustomer) {
        Validate.notBlank(registerCustomer.getName(),"Informe o nome do cliente!");
        Validate.notBlank(registerCustomer.getEmail(),"Informe o email do cliente!");
        Validate.notNull(registerCustomer.getBirthday(),"Informe a data de nasciemento do cliente!");
        Validate.notBlank(registerCustomer.getCpf(),"Informe o cpf do cliente!");
    }
}
