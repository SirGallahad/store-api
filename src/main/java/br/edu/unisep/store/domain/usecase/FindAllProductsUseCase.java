package br.edu.unisep.store.domain.usecase;


import br.edu.unisep.store.data.dao.ProductDao;
import br.edu.unisep.store.domain.builder.ProductBuilder;
import br.edu.unisep.store.domain.dto.ProductDto;

import java.util.List;

public class FindAllProductsUseCase {

    public List<ProductDto> execute () {
        var dao = new ProductDao();
        var products = dao.findAll();

        var builder = new ProductBuilder();
        return builder.from(products);
    }
}
